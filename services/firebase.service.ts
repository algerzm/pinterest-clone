// Import the functions you need from the SDKs you need
import { initializeApp } from '@firebase/app';
import {
  addDoc,
  collection,
  getDocs,
  getFirestore,
  query,
  where,
} from '@firebase/firestore';
import {
  createUserWithEmailAndPassword,
  getAuth,
  GoogleAuthProvider,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signInWithPopup,
} from '@firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyDG0vpcKKTVSnwjxxSittkAH8uDGuL6z2c',
  authDomain: 'pinterest-clone-d019f.firebaseapp.com',
  projectId: 'pinterest-clone-d019f',
  storageBucket: 'pinterest-clone-d019f.appspot.com',
  messagingSenderId: '850747576015',
  appId: '1:850747576015:web:966bc68538d711862131ba',
  measurementId: 'G-H4644M95DQ',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const googleProvider = new GoogleAuthProvider();

const signOut = async () => {
  try {
    await auth.signOut();
    return true;
  } catch (err) {
    return false;
  }
};

const signInWithGoogle = async () => {
  const res = await signInWithPopup(auth, googleProvider);
  const { user } = res;
  const userById = query(collection(db, 'users'), where('uid', '==', user.uid));
  const docs = await getDocs(userById);
  if (docs.docs.length === 0) {
    const resDoc = await addDoc(collection(db, 'users'), {
      uid: user.uid,
      name: user.displayName,
      authProvider: 'google',
      email: user.email,
    });

    if (resDoc) return user;
    return false;
  }

  return user;
};

const logInWithEmailAndPassword = (email: string, password: string) => {
  const resLogin = signInWithEmailAndPassword(auth, email, password);
  return resLogin;
};

const registerWithEmailAndPassword = async (
  name: string,
  email: string,
  password: string
) => {
  const res = await createUserWithEmailAndPassword(auth, email, password);
  const { user } = res;

  const resDoc = await addDoc(collection(db, 'users'), {
    uid: user.uid,
    name,
    authProvider: 'local',
    email,
  });

  if (resDoc) return user;
  return false;
};

const sendPasswordReset = (email: string) => {
  try {
    return sendPasswordResetEmail(auth, email);
  } catch (err) {
    return false;
  }
};

export {
  signInWithGoogle,
  logInWithEmailAndPassword,
  registerWithEmailAndPassword,
  sendPasswordReset,
  signOut,
  auth,
};
