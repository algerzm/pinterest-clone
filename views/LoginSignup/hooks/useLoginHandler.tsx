import { useContext } from 'react';
import { User as FirebaseUser } from '@firebase/auth';
//* SweetAlert2
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { useRouter } from 'next/router';
import {
  logInWithEmailAndPassword,
  signInWithGoogle,
  registerWithEmailAndPassword,
} from '@/services/firebase.service';
import GlobalContext from '@/context/global.context';
import createAdaptedUser from '@/adapters/user.adapter';
import getErrorMessage from '@/utilities/getErrorMessage.util';

const unhandledError = 'auth/unhandled-error';

const useLoginHandler = function useLoginHandler() {
  const router = useRouter();
  const SweetAlert = withReactContent(Swal);

  const globalContext = useContext(GlobalContext);
  const {
    setUser,
    loading,
    startLoading,
    finishLoading,
    authError,
    authErrorMessage,
    setAuthError,
    resetAuthError,
  } = globalContext;

  const saveUser = (user: FirebaseUser) => {
    const adaptedUser = createAdaptedUser(user);
    setUser(adaptedUser);
    localStorage.setItem('user', JSON.stringify(adaptedUser));
    resetAuthError();
    SweetAlert.fire({
      title: 'Success!',
      text: 'You have been authenticated successfully!',
      icon: 'success',
      showConfirmButton: false,
      timer: 1500,
    }).then(() => {
      router.push('/dashboard');
    });
    finishLoading();
  };

  const handleLoginWithEmail = async (email: string, password: string) => {
    startLoading();
    resetAuthError();
    try {
      const res = await logInWithEmailAndPassword(email, password);
      if (res) saveUser(res.user);
      else setAuthError(getErrorMessage(unhandledError));
    } catch (err: any) {
      finishLoading();
      setAuthError(getErrorMessage(err.code));
    }
  };

  const handleSignInWithGoogle = async () => {
    startLoading();
    resetAuthError();
    try {
      const res = await signInWithGoogle();
      if (res) saveUser(res);
      else setAuthError(getErrorMessage(unhandledError));
    } catch (err: any) {
      finishLoading();
      setAuthError(getErrorMessage(err.code));
    }
  };

  const handleSignUpWithEmail = async (
    name: string,
    email: string,
    password: string
  ) => {
    startLoading();
    resetAuthError();
    try {
      const res = await registerWithEmailAndPassword(name, email, password);
      if (res) saveUser(res);
      else setAuthError(getErrorMessage(unhandledError));
    } catch (err: any) {
      finishLoading();
      setAuthError(getErrorMessage(err.code));
    }
  };

  return {
    handleSignInWithGoogle,
    handleLoginWithEmail,
    handleSignUpWithEmail,
    loading,
    authError,
    authErrorMessage,
  };
};

export default useLoginHandler;
