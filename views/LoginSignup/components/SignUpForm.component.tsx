import React, { useContext } from 'react';
//* Form Validation
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { signupSchema } from '@/utilities/validations/signup.validation';
//* Context
import GlobalContext from '@/context/global.context';
//* Components
import FormInput from '@/components/FormInput.component';
import CheckboxInput from '@/components/CheckboxInput.component';
import FormButton from '@/components/FormButton.component';
import OutlinedFormButton from '@/components/OutlinedFormButton.component';
import ButtonLoader from './ButtonLoader.component';
import FormError from './FormError.component';
//* Hooks
import useLoginHandler from '../hooks/useLoginHandler';

const SignUpForm = function SignUpForm() {
  const { toggleAuthForm } = useContext(GlobalContext);
  const { handleSignUpWithEmail, loading, authError, authErrorMessage } =
    useLoginHandler();

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: 'onChange', resolver: yupResolver(signupSchema) });

  const onSubmit = (data: any) => {
    const { name, email, password } = data;
    handleSignUpWithEmail(name, email, password);
  };

  return (
    <>
      <div className="flex gap-3 flex-col form-component">
        <FormInput
          register={register}
          error={Boolean(errors.name)}
          helperText={errors.name?.message}
          name="name"
          placeholder="Name"
        />
        <FormInput
          register={register}
          error={Boolean(errors.email)}
          helperText={errors.email?.message}
          name="email"
          placeholder="Email"
        />
        <FormInput
          register={register}
          error={Boolean(errors.password)}
          helperText={errors.password?.message}
          name="password"
          placeholder="Password"
          password
        />
        <CheckboxInput
          register={register}
          name="privacypolicy"
          id="privacypolicy"
        >
          <p>
            I agree the <span className="text-[#ee6060]">Terms</span> and{' '}
            <span className="text-[#ee6060]">Privacy Policy</span>.
          </p>
        </CheckboxInput>
      </div>
      <div className="flex gap-4 form-component">
        {loading ? (
          <ButtonLoader />
        ) : (
          <>
            <FormButton
              bgColor="#ff5455"
              textColor="white"
              label="Sign Up"
              disabled={!isValid}
              id="signup-button"
              onClick={handleSubmit(onSubmit)}
            />
            <OutlinedFormButton
              mainColor="#ff5455"
              label="Sign In"
              onClick={() => toggleAuthForm()}
              id="go-login-button"
            />
          </>
        )}
      </div>
      {authError && authErrorMessage ? (
        <FormError errorMessage={authErrorMessage} />
      ) : null}
    </>
  );
};

export default SignUpForm;
