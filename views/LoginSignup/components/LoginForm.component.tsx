import React, { useContext } from 'react';
//* Form Validation
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { loginSchema } from '@/utilities/validations/login.validation';
//* Context
import GlobalContext from '@/context/global.context';
//* Components
import FormInput from '@/components/FormInput.component';
import FormButton from '@/components/FormButton.component';
import OutlinedFormButton from '@/components/OutlinedFormButton.component';
import ButtonLoader from './ButtonLoader.component';
import FormError from './FormError.component';
// *Hooks
import useLoginHandler from '../hooks/useLoginHandler';

const SignUpForm = function SignUpForm() {
  const { toggleAuthForm } = useContext(GlobalContext);
  const { handleLoginWithEmail, loading, authError, authErrorMessage } =
    useLoginHandler();

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: 'onChange', resolver: yupResolver(loginSchema) });

  const onSubmit = (data: any) => {
    const { email, password } = data;
    handleLoginWithEmail(email, password);
  };

  return (
    <>
      <div className="flex gap-3 flex-col form-component">
        <FormInput
          register={register}
          error={Boolean(errors.email)}
          helperText={errors.email?.message}
          name="email"
          placeholder="Email"
        />
        <FormInput
          register={register}
          error={Boolean(errors.password)}
          helperText={errors.password?.message}
          name="password"
          placeholder="Password"
          password
        />
      </div>
      <div className="flex gap-4 mt-2 form-component">
        {loading ? (
          <ButtonLoader />
        ) : (
          <>
            <FormButton
              bgColor="#ff5455"
              textColor="white"
              label="Sign In"
              onClick={handleSubmit(onSubmit)}
              disabled={!isValid}
              id="signin-button"
            />
            <OutlinedFormButton
              mainColor="#ff5455"
              label="Sign Up"
              onClick={() => toggleAuthForm()}
              id="go-signup-button"
            />
          </>
        )}
      </div>
      {authError && authErrorMessage ? (
        <FormError errorMessage={authErrorMessage} />
      ) : null}
    </>
  );
};

export default SignUpForm;
