import React from 'react';
//* Icons
import { FaGoogle, FaFacebookF, FaTwitter, FaGithub } from 'react-icons/fa';
//* Components
import FormHeaderButton from './FormHeaderButton.component';
//* Models
import useLoginHandler from '../hooks/useLoginHandler';

interface FormHeaderProps {
  authForm: string;
}

const FormHeader = function FormHeader({ authForm }: FormHeaderProps) {
  const { handleSignInWithGoogle } = useLoginHandler();

  const getTitle = (authFormStr: string) => {
    if (authFormStr === 'SIGNUP') return 'Create Account';
    return 'Login with';
  };

  const getSubtitle = (authFormStr: string) => {
    if (authFormStr === 'SIGNUP') return 'or use your email for registration:';
    return 'or use your email for login:';
  };

  return (
    <>
      <div className="text-center">
        <h3 className="text-[#ee6060] font-bold text-4xl">
          {getTitle(authForm)}
        </h3>
      </div>
      <div className="flex gap-4 justify-center">
        <FormHeaderButton onClickFn={handleSignInWithGoogle} Icon={FaGoogle} />
        <FormHeaderButton Icon={FaFacebookF} />
        <FormHeaderButton Icon={FaTwitter} />
        <FormHeaderButton Icon={FaGithub} />
      </div>
      <div className="text-center">
        <p>{getSubtitle(authForm)}</p>
      </div>
    </>
  );
};

export default FormHeader;
