import React from 'react';
//* Components
import Spinner from '@/components/Spinner.component';

const ButtonLoader = function ButtonLoader() {
  return (
    <div className="w-full flex content-center justify-center">
      <Spinner />
    </div>
  );
};

export default ButtonLoader;
