import React from 'react';

const SideImage = function SideImageComponent() {
  return (
    <div className="bg-womens h-auto bg-cover lg:w-2/5 float-right sm:hidden md:hidden lg:block" />
  );
};

export default SideImage;
