import React from 'react';

interface FormContainerProps {
  children: React.ReactNode;
}

const FormContainer = function FormContainer({ children }: FormContainerProps) {
  return (
    <form className="h-full flex py-10 px-16 gap-8 flex-col lg:w-3/5 md:w-full">
      {children}
    </form>
  );
};

export default FormContainer;
