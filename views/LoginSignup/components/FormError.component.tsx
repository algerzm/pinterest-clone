import React from 'react';

interface FormErrorProps {
  errorMessage: string;
}

const FormError = function FormError({ errorMessage }: FormErrorProps) {
  return (
    <div className="w-full p-3 bg-red-500 text-white text-sm font-semibold text-center flex justify-center items-center">
      <p className="tracking-wide">{errorMessage}</p>
    </div>
  );
};

export default FormError;
