import React from 'react';

interface MainBoxProps {
  children: React.ReactNode;
}

const MainBox = function MainBox({ children }: MainBoxProps) {
  return (
    <div className="w-3/6 border-2 shadow-md bg-white rounded-lg flex max-w-4xl min-w-20 min-h-30">
      {children}
    </div>
  );
};

export default MainBox;
