/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { IconType } from 'react-icons';

interface FormHeaderButtonProps {
  Icon: IconType;
  onClickFn?: () => void;
}

const FormHeaderButton = function FormHeaderButton({
  Icon,
  onClickFn,
}: FormHeaderButtonProps) {
  return (
    <button
      type="button"
      className="rounded-full bg-white border-2 border-black w-8 h-8 flex content-center items-center justify-center"
      onClick={onClickFn}
    >
      <Icon className="w-5 h-5" />
    </button>
  );
};

FormHeaderButton.defaultProps = {
  onClickFn: () => null,
};

export default FormHeaderButton;
