/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
//* Context
import GlobalContext from '@/context/global.context';
//* Components
import MainBox from './components/MainBox.component';
import FormContainer from './components/FormContainer.component';
import SignUpForm from './components/SignUpForm.component';
import FormHeader from './components/FormHeader.component';
import SideImage from './components/SideImage.component';
import LoginForm from './components/LoginForm.component';
//* Hooks
import useIsLoggedIn from '@/hooks/useIsLoggedIn';

const LoginView = function LoginComponent() {
  const { authForm } = useContext(GlobalContext);

  const router = useRouter();
  const { isLogged } = useIsLoggedIn();

  useEffect(() => {
    if (isLogged()) router.push('/dashboard');
  }, []);

  return (
    <div className="flex h-screen justify-center items-center">
      <MainBox>
        <FormContainer>
          <FormHeader authForm={authForm} />
          {authForm === 'SIGNUP' ? <SignUpForm /> : <LoginForm />}
        </FormContainer>
        <SideImage />
      </MainBox>
    </div>
  );
};

export default LoginView;
