/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect, useRef, useCallback } from 'react';
//* Router
import { useRouter } from 'next/router';
//* Utilities
import getId from '../../utilities/idGenerator.util';
//* Components
import ImageCard from './components/ImageCard.component';
import SideNavBar from './components/SideNavBar.component';
import SearchBarContainer from './components/SearchBarContainer.component';
import SearchBar from './components/SearchBar.component';
import Spinner from '@/components/Spinner.component';
import FullImageModal from './components/FullImageModal.component';
//* Services
import { AdaptedImage } from '@/models';
import useLoadImages from './hooks/useLoadImages';
//* Hooks
import useIsLoggedIn from '@/hooks/useIsLoggedIn';

interface DashboardViewProps {
  images: AdaptedImage[];
}

const DashboardView = function DashboardComponent({
  images,
}: DashboardViewProps) {
  //* Load Images Hook
  const router = useRouter();
  const [page, setPage] = useState(1);
  const [query, setQuery] = useState('valorant');
  const { loading, shownImages, error } = useLoadImages(page, images, query);
  const loader = useRef(null);

  //* Full Image Modal
  const [isOpen, setIsOpen] = useState(false);
  const [currentImage, setCurrentImage] = useState<string>('');

  const { isLogged } = useIsLoggedIn();

  const showModal = (imageId: string) => {
    setCurrentImage(imageId);
    setIsOpen(true);
    const { body } = document;
    body.style.overflow = 'hidden';
  };

  const changeQuery = (newQuery: string) => {
    setPage(2);
    setQuery(newQuery);
  };

  const handleObserver = useCallback(
    (entries: Array<IntersectionObserverEntry>) => {
      const target = entries[0];
      if (target.isIntersecting) {
        setPage((prev) => prev + 1);
      }
    },
    []
  );

  useEffect(() => {
    if (!isLogged()) router.push('/login');
    const options = {
      root: null,
      rootMargin: '20px',
      threshold: 0,
    };
    const observer = new IntersectionObserver(handleObserver, options);
    if (loader.current) observer.observe(loader.current);
  }, [handleObserver, router]);

  return (
    <div className="flex bg-white" id="MainDashboard">
      <FullImageModal
        currentImage={currentImage}
        isOpen={isOpen}
        setIsOpen={setIsOpen}
      />
      <div className="flex-none w-0 md:w-40 lg:w-60 h-full sm:hidden md:block">
        <SideNavBar />
      </div>
      <div className="flex-1 px-8 md:px-16 md:pl-0 lg:pr-20 flex flex-col">
        <SearchBarContainer>
          <SearchBar changeQuery={changeQuery} />
        </SearchBarContainer>
        <div className="columns-2 md:columns-2xs gap-5 w-full z-0">
          {shownImages.length
            ? shownImages.map((image) => (
                <ImageCard
                  key={getId()}
                  imageUrl={image.link}
                  imageId={image.id}
                  aspect={image.width > image.height ? 'video' : 'square'}
                  onClickFn={showModal}
                />
              ))
            : null}
        </div>
        <div
          className="w-full h-10 relative mt-10 mb-10 flex justify-center content-center"
          ref={loader}
        >
          {loading && <Spinner />}
          {error && <p className="text-2xl font-bold">No images left :c</p>}
        </div>
      </div>
    </div>
  );
};

export default DashboardView;
