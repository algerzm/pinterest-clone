import React from 'react';

interface SearchBarContainerProps {
  children: React.ReactNode;
}

const SearchBarContainer = function SearchBarContainer({
  children,
}: SearchBarContainerProps) {
  return (
    <div className="flex sticky top-0 bg-white pt-10 pb-10 z-10">
      {children}
    </div>
  );
};

export default SearchBarContainer;
