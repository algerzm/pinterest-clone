/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { IconType } from 'react-icons';

interface NavBarButtonProps {
  ButtonIcon: IconType;
  dark?: boolean;
  onClickFn?: () => void;
  title: string;
}

const NavBarButton = function NavBarButton({
  ButtonIcon,
  dark,
  onClickFn,
  title,
}: NavBarButtonProps) {
  return (
    <button
      type="button"
      className={`w-12 h-12 border-2 border-gray-900 rounded-full flex justify-center content-center items-center shadow-xl hover:scale-105 transition-all ${
        dark && 'bg-gray-900 text-white'
      }`}
      title={title}
      onClick={onClickFn}
    >
      <ButtonIcon className="w-6 text-xl" />
    </button>
  );
};

NavBarButton.defaultProps = {
  dark: false,
  onClickFn: () => null,
};

export default NavBarButton;
