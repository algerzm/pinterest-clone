/* eslint-disable @next/next/no-img-element */
import React, { useContext } from 'react';
//* Icons
import { FaUserAlt, FaQuestion, FaSignOutAlt } from 'react-icons/fa';
import { MdNotifications } from 'react-icons/md';
import { IoMdChatbubbles } from 'react-icons/io';
import { AiFillHeart } from 'react-icons/ai';
//* Router
import { useRouter } from 'next/router';
//* Context
import GlobalContext from '@/context/global.context';
//* Components
import NavBarButton from './NavBarButton.component';
//* Firebase
import { signOut } from '@/services/firebase.service';

const SideNavBar = function SideNavBar() {
  const router = useRouter();
  const { resetUser } = useContext(GlobalContext);

  const handleLogout = () => {
    localStorage.clear();
    resetUser();
    signOut();
    router.push('/login');
  };

  return (
    <nav className="flex flex-col gap-8 fixed md:left-10 lg:left-20">
      <div className="hidden md:flex justify-center content-center pt-10">
        <img
          src="https://graffica.info/wp-content/uploads/2017/08/badgeRGB.png"
          alt="pinteres logo"
          className="w-14"
        />
      </div>
      <div className="hidden md:flex flex-col justify-center items-center pt-10 gap-6">
        <NavBarButton title="Account" ButtonIcon={FaUserAlt} />
        <NavBarButton title="Notifications" ButtonIcon={MdNotifications} />
        <NavBarButton title="Messages" ButtonIcon={IoMdChatbubbles} />
      </div>
      <div className="hidden md:flex flex-col justify-center items-center pt-10 gap-6">
        <NavBarButton title="Favorites" dark ButtonIcon={AiFillHeart} />
      </div>
      <div className="hidden md:flex flex-col justify-center items-center pt-10 gap-6">
        <NavBarButton title="FAQ" ButtonIcon={FaQuestion} />
        <NavBarButton
          title="Sign Out"
          onClickFn={handleLogout}
          ButtonIcon={FaSignOutAlt}
        />
      </div>
    </nav>
  );
};

export default SideNavBar;
