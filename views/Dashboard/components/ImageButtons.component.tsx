/* eslint-disable @next/next/no-img-element */
import React from 'react';

const ImageButtons = function ImageButtons() {
  return (
    <div className="flex gap-3">
      <button
        type="button"
        className="w-10 h-10 bg-red-600 rounded-full flex justify-center items-center"
      >
        <img
          src="https://www.chemicalsafetyfacts.org/wp-content/uploads/2021/01/heart-icon-white.png"
          alt="heart"
          className="w-6 h-6"
        />
      </button>
      <button
        type="button"
        className="w-10 h-10 bg-red-600 rounded-full flex justify-center items-center"
      >
        <img
          src="https://icon-library.com/images/white-plus-icon/white-plus-icon-3.jpg"
          alt="heart"
          className="w-6 h-6"
        />
      </button>
      <button
        type="button"
        className="w-10 h-10 bg-red-600 rounded-full flex justify-center items-center"
      >
        <img
          src="https://www.seekpng.com/png/full/521-5211467_alcohol-and-the-liver-share-icon-white-transparent.png"
          alt="heart"
          className="w-6 h-6"
        />
      </button>
    </div>
  );
};

export default ImageButtons;
