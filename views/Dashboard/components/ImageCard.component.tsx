/* eslint-disable @next/next/no-img-element */
import React from 'react';
import styles from '../styles/ImageCard.module.css';
//* Router
// import { useRouter } from 'next/router';
//* Components
import ImageButtons from './ImageButtons.component';

interface ImageCardProps {
  aspect: string;
  imageUrl: string;
  imageId: string;
  onClickFn: (id: string) => void;
}

const ImageCard = function ImageCard({
  aspect,
  imageUrl,
  imageId,
  onClickFn,
}: ImageCardProps) {
  /*  const router = useRouter();

 const handleClick = () => {
    router.push(`/image/${imageId}`);
  }; */
  return (
    <div className={styles.imageContainer}>
      <img
        src={imageUrl}
        alt="beach"
        style={{
          aspectRatio: aspect === 'video' ? '16/9' : '1/1',
          width: '100%',
          height: '100%',
          objectFit: 'cover',
          borderRadius: '1.5rem',
          marginBottom: '0.75rem',
        }}
      />
      <div
        aria-hidden
        className={styles.after}
        onClick={() => onClickFn(imageId)}
      >
        <ImageButtons />
      </div>
    </div>
  );
};

export default ImageCard;
