/* eslint-disable @next/next/no-img-element */
import React, { useState, ChangeEvent } from 'react';
//* Icons
import { IoArrowRedo } from 'react-icons/io5';

interface SearchBarProps {
  changeQuery: (newQuery: string) => void;
}

const SearchBar = function SearchBar({ changeQuery }: SearchBarProps) {
  const [searchText, setSearchText] = useState('');

  const handleSearchChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchText(e.target.value);
  };

  const handleKeyEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      changeQuery(searchText);
    }
  };

  return (
    <>
      <input
        type="text"
        className="w-full h-12 rounded-2xl border-1 border-gray-900 px-8 font-medium text-sm shadow-2xl tracking-wider"
        placeholder="Search"
        value={searchText}
        onChange={handleSearchChange}
        onKeyUp={handleKeyEnter}
      />
      <button
        type="button"
        className="w-8 h-8 -ml-12 mt-2 bg-gray-900 rounded-full flex items-center justify-center text-white"
        onClick={() => changeQuery(searchText)}
      >
        <IoArrowRedo className="text-lg" />
      </button>
    </>
  );
};

export default SearchBar;
