import React, { useState, useEffect } from 'react';
//* Components
import Modal from 'react-modal';
//* Views
import FullSizeImage from '@/views/FullSizeImage/FullSizeImage.view';

interface FullSizeImageModalProps {
  setIsOpen: (isOpen: boolean) => void;
  isOpen: boolean;
  currentImage: string;
}

const FullImageModal = function FullImageModal({
  setIsOpen,
  isOpen,
  currentImage,
}: FullSizeImageModalProps) {
  const [safeDocument, setSafeDocument] = useState<any>(null);

  useEffect(() => {
    if (document && !safeDocument) {
      setSafeDocument(document);
    }
  }, [safeDocument]);

  const closeModal = () => {
    setIsOpen(false);
    const { body } = document;
    body.style.overflow = 'scroll';
    body.style.overflowX = 'hidden';
  };

  return safeDocument ? (
    <Modal
      isOpen={isOpen}
      onAfterClose={() => closeModal()}
      appElement={safeDocument.getElementById('root') as HTMLElement}
      ariaHideApp={false}
      style={{
        overlay: {
          zIndex: '100',
          position: 'fixed',
          animation: '0.2s ease-out 0s 1 visible',
        },
      }}
    >
      {currentImage && (
        <FullSizeImage
          imageId={currentImage}
          closeModalFn={() => closeModal()}
        />
      )}
    </Modal>
  ) : null;
};

export default FullImageModal;
