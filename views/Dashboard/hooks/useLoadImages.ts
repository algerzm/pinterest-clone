/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useCallback } from 'react';
import { AxiosResponse } from 'axios';
import { AdaptedImage } from '@/models';
import { getImageByQuery } from '../services/images.service';
import extractGalleryImages from '@/adapters/gallery.adapter';
import createAdaptedImages from '@/adapters/images.adapter';

const useLoadImages = function useLoadImages(
  page: number,
  initialList: AdaptedImage[],
  query: string
) {
  const [controlQuery, setControlQuery] = useState(query);
  const [storedImages, setStoredImages] = useState<AdaptedImage[]>(initialList);
  const [shownImages, setShownImages] = useState<AdaptedImage[]>(
    [...initialList].slice(0, 1 * 10)
  );
  const [galleryPage, setGalleryPage] = useState(2);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const sendQuery = useCallback(
    async (queryString: string, galleryPageParam: number) => {
      try {
        setLoading(true);
        setError(false);
        const res: AxiosResponse = await getImageByQuery(
          queryString,
          galleryPageParam
        );
        const gallerry = res.data.data;

        const images = extractGalleryImages(gallerry);
        const adaptedImages = createAdaptedImages(images);
        if (adaptedImages.length !== 0) {
          setStoredImages((prev) => [...prev, ...adaptedImages]);
          setGalleryPage(galleryPageParam + 1);
          setLoading(false);
        } else {
          setError(true);
          setLoading(false);
        }
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    },
    [storedImages]
  );

  useEffect(() => {
    const clearStates = () => {
      Promise.all([
        setStoredImages([]),
        setShownImages([]),
        setGalleryPage(1),
        setControlQuery(query),
      ]);
    };

    if (
      page * 10 < storedImages.length &&
      page !== 1 &&
      controlQuery === query
    ) {
      setLoading(true);
      setShownImages([...storedImages].slice(0, page * 10));
      setLoading(false);
    } else if (controlQuery !== query) {
      clearStates();
    } else if (page !== 1) {
      sendQuery(query, galleryPage);
    }
  }, [page, storedImages, query]);

  return { loading, error, shownImages };
};

export default useLoadImages;
