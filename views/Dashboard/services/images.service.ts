import axios, { AxiosResponse } from 'axios';
import { ResponseImage } from '@/models';

const BASE_URL = 'https://api.imgur.com/3';
const CLIENT_ID = '323fe85249edd9a';
// const api =('https://api.imgur.com/3/gallery/hot/top/all/1?showViral=true&mature=false&album_previews=false');
/* const CLIENT_SECRET = '17693d1ec1b76ed1b614891deebdf494ffd5c9ab';
  const REFRESH_TOKEN = 'c518654e899274f8bb549e14154ca33ed6ff3dfb';
  const ACCOUNT_ID = '161438861';
  const account_username = 'MrNaviZm';
  const GALLERY = 'https://api.imgur.com/3/gallery/top/hot/all/1'; */

export const getImages = () => {
  return axios.get<ResponseImage[]>(
    `${BASE_URL}/gallery/search/viral/all/1?q=dog`,
    {
      headers: {
        Authorization: `Client-ID ${CLIENT_ID}`,
      },
    }
  );
};

export const getImageByQuery = (query: string, page: number) => {
  return axios.get<ResponseImage[]>(
    `${BASE_URL}/gallery/search/top/all/${page}?q=${query}`,
    {
      headers: {
        Authorization: `Client-ID ${CLIENT_ID}`,
      },
    }
  );
};

export const getImageById = (id: string) => {
  return axios.get<AxiosResponse>(`${BASE_URL}/image/${id}`, {
    headers: {
      Authorization: `Client-ID ${CLIENT_ID}`,
    },
  });
};
