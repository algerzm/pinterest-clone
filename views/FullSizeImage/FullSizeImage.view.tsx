/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react';
//* Hooks
import { useRouter } from 'next/router';
import useIsLoggedIn from '@/hooks/useIsLoggedIn';
//* Components
import FAB from '@/components/FAB.component';
import MainContainer from './components/MainContainer.component';
import SideImage from './components/SideImage.component';
import ImageInfoContainer from './components/ImageInfo.component';
import ImageHeader from './components/ImageHeader.component';
import ImageComments from './components/ImageComments.component';
//* Services
import { getImageById } from '../Dashboard/services/images.service';
//* Adapters
import createAdaptedImages from '@/adapters/images.adapter';
import { AdaptedImage } from '@/models';

const FullSizeImage = function FullSizeImage({ imageId, closeModalFn }: any) {
  const [image, setImage] = useState<AdaptedImage>();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { isLogged } = useIsLoggedIn();

  useEffect(() => {
    if (!isLogged()) router.push('/login');
    const getImage = async () => {
      const resImage = await getImageById(imageId);
      const nImage = resImage.data.data;
      const adaptedImage = createAdaptedImages([nImage]);
      setImage(adaptedImage[0]);
      setLoading(false);
    };
    if (!loading && image === undefined) {
      setLoading(true);
      getImage();
    }
  }, []);

  return (
    <>
      <FAB onClickFn={closeModalFn} />
      <MainContainer>
        <div className="w-full h-full flex flex-col lg:flex-row gap-2">
          <SideImage loading={loading} image={image} />
          <ImageInfoContainer>
            <ImageHeader loading={loading} image={image} />
            <ImageComments />
          </ImageInfoContainer>
        </div>
      </MainContainer>
    </>
  );
};

export default FullSizeImage;
