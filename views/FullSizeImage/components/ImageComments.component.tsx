import React from 'react';

const ImageComments = function ImageComments() {
  return (
    <div className="hidden md:block mb-8 px-8">
      <h2 className="text-lg mt-4 lg:text-2xl font-bold mb-4 lg:mb-8">
        Comentarios:
      </h2>
      <div className="ml-4 flex flex-col gap-4">
        <p className="text-md">
          <span className="font-bold text-md lg:text-lg mr-2">AlgerIvan </span>
          This in an awesome comment from me
        </p>
        <p className="text-md">
          <span className="font-bold text-md lg:text-lg mr-2">MrNavi </span>I
          love this image
        </p>
        <p className="text-md">
          <span className="font-bold text-md lg:text-lg mr-2">Jessica123 </span>
          Awesomeeee!
        </p>
      </div>
    </div>
  );
};

export default ImageComments;
