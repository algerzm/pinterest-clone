import React from 'react';

interface ImageInfoContainerProps {
  children: React.ReactNode;
}

const ImageInfoContainer = function ImageInfoContainer({
  children,
}: ImageInfoContainerProps) {
  return (
    <div className="h-full border-2 shadow-xl bg-white rounded-lg w-full lg:w-2/6 flex flex-col pt-8 justify-between">
      {children}
    </div>
  );
};

export default ImageInfoContainer;
