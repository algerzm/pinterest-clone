/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { AdaptedImage } from '@/models';
import Spinner from '@/components/Spinner.component';

interface SideImageProps {
  loading: boolean;
  image: AdaptedImage | undefined;
}

const SideImage = function SideImage({ loading, image }: SideImageProps) {
  return (
    <div className="h-full border-2 shadow-md bg-white rounded-lg w-full lg:w-4/6 flex justify-center items-center p-4">
      {loading && <Spinner />}
      {image && (
        <img
          src={image.link}
          alt="valorant"
          className="w-full h-full object-contain"
        />
      )}
    </div>
  );
};

export default SideImage;
