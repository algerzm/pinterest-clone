import React from 'react';
import { AdaptedImage } from '@/models';
//* Components
import Spinner from '@/components/Spinner.component';

interface ImageHeaderProps {
  image: AdaptedImage | undefined;
  loading: boolean;
}

const ImageHeader = function ImageHeader({ image, loading }: ImageHeaderProps) {
  return (
    <div>
      <h2 className="text-lg md:text-2xl font-bold mb-8 bg-red-600 py-4 px-2 text-white shadow-inner">
        {loading && <Spinner />}
        {image?.title ? image.title : 'Image'}
      </h2>
      <p className="px-8 text-md lg:text-lg">
        {loading && <Spinner />}
        {image?.description ? image.description : null}
      </p>
    </div>
  );
};

export default ImageHeader;
