import React from 'react';

interface MainContainerProps {
  children: React.ReactNode;
}

const MainContainer = function MainContainer({ children }: MainContainerProps) {
  return (
    <div className="h-full flex justify-center content-center items-center p-6 md:p-12">
      {children}
    </div>
  );
};

export default MainContainer;
