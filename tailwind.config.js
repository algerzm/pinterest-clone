module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './views/**/*.{js,ts,jsx,tsx}',
    './views/**/components/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        womens: "url('../public/login/womens4.png')",
      },
      maxHeight: {
        128: '38rem',
        20: '20rem',
      },
      minWidth: {
        10: '10rem',
        20: '25rem',
      },
      minHeight: {
        30: '36rem',
      },
      borderWidth: {
        1: '1.5px',
      },
    },
  },
  plugins: [],
};
