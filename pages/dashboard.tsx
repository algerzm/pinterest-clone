/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import type { NextPage } from 'next';
import { AxiosResponse } from 'axios';
// * View
import DashboardView from '@/views/Dashboard/Dashboard.view';
//* Adapters
import createAdaptedImages from '@/adapters/images.adapter';
//* Services
import { getImageByQuery } from '@/views/Dashboard/services/images.service';
//* Utilities
import extractGalleryImages from '@/adapters/gallery.adapter';

const Dashboard: NextPage = function DashboardPage({ images }: any) {
  return (
    <div id="MainComponent">
      <DashboardView images={images} />
    </div>
  );
};

export async function getServerSideProps() {
  const res: AxiosResponse = await getImageByQuery('valorant', 1);
  const gallerry = res.data.data;

  const images = extractGalleryImages(gallerry);
  const adaptedImages = createAdaptedImages(images);
  return {
    props: {
      images: adaptedImages,
    },
  };
}

export default Dashboard;
