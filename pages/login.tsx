import React from 'react';
import type { NextPage } from 'next';
import LoginView from '@/views/LoginSignup/Login.view';

const loginPage: NextPage = function LoginPage() {
  return <LoginView />;
};
export default loginPage;
