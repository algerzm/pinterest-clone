import React from 'react';
import { FieldValues, UseFormRegister } from 'react-hook-form';

interface CheckboxInputProps {
  name: string;
  id: string;
  children: React.ReactNode;
  register: UseFormRegister<FieldValues>;
}

const CheckboxInput = function CheckboxInput({
  name,
  id,
  children,
  register,
}: CheckboxInputProps) {
  return (
    <div className="flex flex-row items-center gap-3 mt-2">
      <input
        type="checkbox"
        id={id}
        className="w-5 h-5 accent-[#ff5455] border-0 rounded-md focus:ring-0 text-sm"
        {...register(name)}
      />
      {children}
    </div>
  );
};

export default CheckboxInput;
