import React from 'react';

interface FormButtonProps {
  bgColor: string;
  textColor: string;
  label: string;
  onClick: () => void;
  disabled?: boolean;
  id: string;
}

const FormButton = function FormButton({
  bgColor,
  textColor,
  label,
  onClick,
  disabled,
  id,
}: FormButtonProps) {
  return (
    <button
      type="button"
      style={{
        backgroundColor: disabled ? '#fb9d9d' : bgColor,
        color: disabled ? '#fefbfb' : textColor,
        cursor: disabled ? 'not-allowed' : 'pointer',
      }}
      disabled={disabled}
      className="h-11 w-2/4 rounded-lg font-bold text-sm"
      onClick={() => onClick()}
      id={id}
    >
      {label}
    </button>
  );
};

FormButton.defaultProps = {
  disabled: false,
};

export default FormButton;
