import React from 'react';

interface FormButtonProps {
  mainColor: string;
  label: string;
  onClick: () => void;
  disabled?: boolean;
  id: string;
}

const OutlinedFormButton = function OutlinedFormButton({
  mainColor,
  label,
  onClick,
  disabled,
  id,
}: FormButtonProps) {
  return (
    <button
      type="button"
      style={{
        backgroundColor: 'bg-white',
        color: mainColor,
        border: `2px solid ${mainColor}`,
      }}
      className="h-11 w-2/4 rounded-lg font-bold text-sm"
      onClick={() => onClick()}
      disabled={disabled}
      id={id}
    >
      {label}
    </button>
  );
};

OutlinedFormButton.defaultProps = {
  disabled: false,
};

export default OutlinedFormButton;
