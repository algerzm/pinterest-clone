import React from 'react';
import { FieldValues, UseFormRegister } from 'react-hook-form';
//* Components
import HelperTextLabel from './HelperTextLabel.component';

interface FormHeaderButtonProps {
  name: string;
  placeholder: string;
  error?: boolean;
  register: UseFormRegister<FieldValues>;
  helperText?: string;
  password?: boolean;
}

const FormInput = function FormInput({
  name,
  placeholder,
  error,
  register,
  helperText,
  password,
}: FormHeaderButtonProps) {
  return (
    <div>
      <input
        type={password ? 'password' : 'text'}
        className={`w-full h-11 text-gray-700 bg-gray-100 border-2 rounded-md px-4 py-2 text-sm ${
          error
            ? 'border-red-600 focus: outline-red-600'
            : 'focus:outline-slate-300'
        }`}
        placeholder={placeholder}
        {...register(name)}
        autoComplete="off"
      />
      {error ? <HelperTextLabel helperText={helperText} name={name} /> : null}
    </div>
  );
};

FormInput.defaultProps = {
  error: false,
  helperText: '',
  password: false,
};

export default FormInput;
