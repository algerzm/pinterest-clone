import React from 'react';
import { IoIosArrowBack } from 'react-icons/io';

interface FABprops {
  onClickFn?: () => void;
}

const FAB = function FAB({ onClickFn }: FABprops) {
  return (
    <button
      onClick={onClickFn}
      type="button"
      className="p-0 w-10 h-10 bg-red-600 rounded-full hover:bg-red-700 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none fixed top-8 left-8"
    >
      <IoIosArrowBack className="text-3xl text-white ml-1" />
    </button>
  );
};

FAB.defaultProps = {
  onClickFn: () => null,
};

export default FAB;
