import React from 'react';

interface HelperTextLabelProps {
  name: string;
  helperText?: string;
}

const HelperTextLabel = function HelperTextLabel({
  name,
  helperText,
}: HelperTextLabelProps) {
  return (
    <label className="text-xs text-red-500" htmlFor={name}>
      {helperText}
    </label>
  );
};

HelperTextLabel.defaultProps = {
  helperText: '',
};

export default HelperTextLabel;
