/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    outputStandalone: true,
  },
  reactStrictMode: false,
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
  images: {
    domains: ['www.planetware.com'],
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/login',
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
