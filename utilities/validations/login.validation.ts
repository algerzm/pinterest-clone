import { object, string, ObjectSchema } from 'yup';

const loginSchema: ObjectSchema<any> = object().shape({
  email: string()
    .required('Email is required')
    .email('Email must be a valid email'),
  password: string()
    .required('Password is required')
    .min(8, 'Password must be at least 8 characters'),
});

export { loginSchema };
