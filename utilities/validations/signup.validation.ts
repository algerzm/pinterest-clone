import { object, string, ObjectSchema, bool } from 'yup';

const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

const signupSchema: ObjectSchema<any> = object().shape({
  name: string()
    .required('Username is required')
    .min(5, 'Username must be at least 5 characters'),
  email: string().email('Email must be a valid email'),
  password: string()
    .required('Password is required')
    .min(8, 'Password must be at least 8 characters')
    .matches(
      passwordRegex,
      'Password must include lower, upper case letters and numbers'
    ),
  privacypolicy: bool().oneOf([true], 'You must agree to the privacy policy'),
});

export { signupSchema };
