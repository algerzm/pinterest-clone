import { v4 as uuid } from 'uuid';

function getId(): string {
  return uuid();
}

export default getId;
