import { useContext } from 'react';
import GlobalContext from '@/context/global.context';
import { User } from '@/models';

const useIsLoggedIn = () => {
  const globalContext = useContext(GlobalContext);
  const { setUser, user } = globalContext;

  function isLogged() {
    if (!user?.id) {
      const localUser = localStorage.getItem('user');
      const nUser: User = localUser ? JSON.parse(localUser) : null;
      if (nUser) {
        setUser(nUser);
        return true;
      }
      return false;
    }

    return true;
  }

  return { isLogged };
};

export default useIsLoggedIn;
