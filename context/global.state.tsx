import React, { useReducer, useMemo } from 'react';

import GlobalContext from './global.context';
import GlobalReducer from './global.reducer';
//* Models
import { GlobalState } from '@/models/global-state.model';
import { User } from '@/models';

//* Actions
import {
  SET_USER,
  RESET_USER,
  TOOGLE_AUTH_METHOD,
  FINISH_LOADING,
  START_LOADING,
  SET_AUTH_ERROR,
  RESET_AUTH_ERROR,
} from './action.types';

interface GlobalStateProps {
  children: React.ReactNode;
}

const GlobalStateProvider = function GlobalStateProvider({
  children,
}: GlobalStateProps) {
  const initialState: GlobalState = {
    user: {
      id: '',
      name: '',
      email: '',
      refreshToken: '',
    },
    authForm: 'SIGNUP',
    loading: false,
    authError: false,
    authErrorMessage: '',
  };

  const [state, dispatch] = useReducer(GlobalReducer, initialState);

  //* Functions that modifies the state
  const setUser = (user: User) => {
    dispatch({
      type: SET_USER,
      payload: user,
    });
  };

  const resetUser = () => {
    dispatch({
      type: RESET_USER,
      payload: null,
    });
  };

  const toggleAuthForm = () => {
    dispatch({
      type: TOOGLE_AUTH_METHOD,
      payload: null,
    });
  };

  const startLoading = () => {
    dispatch({
      type: START_LOADING,
      payload: null,
    });
  };

  const finishLoading = () => {
    dispatch({
      type: FINISH_LOADING,
      payload: null,
    });
  };

  const setAuthError = (errorMessage: string) => {
    dispatch({
      type: SET_AUTH_ERROR,
      payload: errorMessage,
    });
  };

  const resetAuthError = () => {
    dispatch({
      type: RESET_AUTH_ERROR,
      payload: null,
    });
  };

  const returnValues = useMemo(
    () => ({
      user: state.user,
      authForm: state.authForm,
      loading: state.loading,
      authErrorMessage: state.authErrorMessage,
      authError: state.authError,
      setUser,
      resetUser,
      toggleAuthForm,
      startLoading,
      finishLoading,
      setAuthError,
      resetAuthError,
    }),
    [
      state.user,
      state.authForm,
      state.loading,
      state.authErrorMessage,
      state.authError,
    ]
  );

  return (
    <GlobalContext.Provider value={returnValues}>
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalStateProvider;
