import { GlobalState } from '@/models/global-state.model';
import { ReducerAction } from '@/models';
import {
  SET_USER,
  RESET_USER,
  TOOGLE_AUTH_METHOD,
  START_LOADING,
  FINISH_LOADING,
  SET_AUTH_ERROR,
  RESET_AUTH_ERROR,
} from './action.types';

const reducer = (state: GlobalState, action: ReducerAction) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.payload,
      };
    case RESET_USER:
      return {
        ...state,
        user: null,
      };
    case TOOGLE_AUTH_METHOD:
      return {
        ...state,
        authForm: state.authForm === 'LOGIN' ? 'SIGNUP' : 'LOGIN',
      };
    case START_LOADING:
      return {
        ...state,
        loading: true,
      };
    case FINISH_LOADING:
      return {
        ...state,
        loading: false,
      };
    case SET_AUTH_ERROR:
      return {
        ...state,
        authError: true,
        authErrorMessage: action.payload,
      };
    case RESET_AUTH_ERROR:
      return {
        ...state,
        authError: false,
        authErrorMessage: '',
      };
    default:
      return state;
  }
};

export default reducer;
