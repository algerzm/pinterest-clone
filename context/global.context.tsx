import { createContext } from 'react';
import { User } from '@/models';

interface ContextTypes {
  user: any;
  authForm: string;
  loading: boolean;
  authErrorMessage: string;
  authError: boolean;
  setUser: (user: User) => void;
  resetUser: () => void;
  toggleAuthForm: () => void;
  startLoading: () => void;
  finishLoading: () => void;
  setAuthError: (errorMessage: string) => void;
  resetAuthError: () => void;
}
const GlobalContext = createContext<ContextTypes>({
  user: null,
  authForm: '',
  loading: false,
  authError: false,
  authErrorMessage: '',
  setUser: (user: User) => user,
  resetUser: () => null,
  toggleAuthForm: () => null,
  startLoading: () => null,
  finishLoading: () => null,
  setAuthError: (errorMessage: string) => errorMessage,
  resetAuthError: () => null,
});

export default GlobalContext;
