/* eslint-disable no-undef */
it('Loads dashboard page succesfully and then return to login because no users are logged in', () => {
  cy.visit('http://localhost:3000/dashboard');
  cy.wait(1000);
  cy.url().should('include', '/login');
});

it('Loads dashboard when user is in localstorage', () => {
  const userInfo =
    '{"id":"P2yuZT5xrdRJQUUhdsn2NBHHODg1","name":"Alger Ivan","email":"algerzm@gmail.com","refreshToken":"AIwUaOmo79B_ragYW2crOOhKjRkESYUoVObBN4LYrGKj05K5E9FdtnvPV6JJNvXErHS8KSo8QQ01w7wHQ-0R7F-y9VyCFC8k8reFtD36HBem5-zdvM1es30EwU-08U_07jcQU6eW3YVlKfmpCKC5SwlMGYdhyr1gvRTqE4ezyOMflXaH-v4EESZJ2egMfgV5UL0NqR7VzCb_h1oiJxEOD-sz7641q8BKnOwA7zIRNB7wnD0HOwbh3x54yRd_fUQVgll0PACJ9CkPau8SkCqdPlpobWNtG0fORLiEoTibG0XwF3tvB8ppMcupKKz6s84NcF1Nhwahp1IU6_0znJ4JmLO8lUWICemPyljYFnz5HU6WGhP5-Z5UOO5-uW0Sp5_Zl7mgWbtUZuu4QIVX3dZd87Ey3nyfR2WxbImgqrYYiHIph0qTeMFQAZA","displayName":"Alger Ivan","photoURL":"https://lh3.googleusercontent.com/a-/AOh14GgE5rB5uKhHf-9lO9XdsVkoo-AwdRFiBCXb17lL9A=s96-c"}';
  cy.visit('http://localhost:3000/dashboard');
  window.localStorage.setItem('user', userInfo);
  cy.wait(1000);
  cy.get('#MainDashboard').should('be.visible');
});

it('Load at least 10 images', () => {
  const userInfo =
    '{"id":"P2yuZT5xrdRJQUUhdsn2NBHHODg1","name":"Alger Ivan","email":"algerzm@gmail.com","refreshToken":"AIwUaOmo79B_ragYW2crOOhKjRkESYUoVObBN4LYrGKj05K5E9FdtnvPV6JJNvXErHS8KSo8QQ01w7wHQ-0R7F-y9VyCFC8k8reFtD36HBem5-zdvM1es30EwU-08U_07jcQU6eW3YVlKfmpCKC5SwlMGYdhyr1gvRTqE4ezyOMflXaH-v4EESZJ2egMfgV5UL0NqR7VzCb_h1oiJxEOD-sz7641q8BKnOwA7zIRNB7wnD0HOwbh3x54yRd_fUQVgll0PACJ9CkPau8SkCqdPlpobWNtG0fORLiEoTibG0XwF3tvB8ppMcupKKz6s84NcF1Nhwahp1IU6_0znJ4JmLO8lUWICemPyljYFnz5HU6WGhP5-Z5UOO5-uW0Sp5_Zl7mgWbtUZuu4QIVX3dZd87Ey3nyfR2WxbImgqrYYiHIph0qTeMFQAZA","displayName":"Alger Ivan","photoURL":"https://lh3.googleusercontent.com/a-/AOh14GgE5rB5uKhHf-9lO9XdsVkoo-AwdRFiBCXb17lL9A=s96-c"}';
  cy.visit('http://localhost:3000/dashboard');
  window.localStorage.setItem('user', userInfo);
  cy.wait(1000);
  cy.get('div.ImageCard_imageContainer__4vDgr').should(
    'have.length.greaterThan',
    9
  );
});

it('Load all 6 side navbar buttons', () => {
  const userInfo =
    '{"id":"P2yuZT5xrdRJQUUhdsn2NBHHODg1","name":"Alger Ivan","email":"algerzm@gmail.com","refreshToken":"AIwUaOmo79B_ragYW2crOOhKjRkESYUoVObBN4LYrGKj05K5E9FdtnvPV6JJNvXErHS8KSo8QQ01w7wHQ-0R7F-y9VyCFC8k8reFtD36HBem5-zdvM1es30EwU-08U_07jcQU6eW3YVlKfmpCKC5SwlMGYdhyr1gvRTqE4ezyOMflXaH-v4EESZJ2egMfgV5UL0NqR7VzCb_h1oiJxEOD-sz7641q8BKnOwA7zIRNB7wnD0HOwbh3x54yRd_fUQVgll0PACJ9CkPau8SkCqdPlpobWNtG0fORLiEoTibG0XwF3tvB8ppMcupKKz6s84NcF1Nhwahp1IU6_0znJ4JmLO8lUWICemPyljYFnz5HU6WGhP5-Z5UOO5-uW0Sp5_Zl7mgWbtUZuu4QIVX3dZd87Ey3nyfR2WxbImgqrYYiHIph0qTeMFQAZA","displayName":"Alger Ivan","photoURL":"https://lh3.googleusercontent.com/a-/AOh14GgE5rB5uKhHf-9lO9XdsVkoo-AwdRFiBCXb17lL9A=s96-c"}';
  cy.visit('http://localhost:3000/dashboard');
  window.localStorage.setItem('user', userInfo);
  cy.wait(1000);
  cy.get('div.items-center > button').should('have.length', 6);
});

it('Should show sticky searchbar on top', () => {
  const userInfo =
    '{"id":"P2yuZT5xrdRJQUUhdsn2NBHHODg1","name":"Alger Ivan","email":"algerzm@gmail.com","refreshToken":"AIwUaOmo79B_ragYW2crOOhKjRkESYUoVObBN4LYrGKj05K5E9FdtnvPV6JJNvXErHS8KSo8QQ01w7wHQ-0R7F-y9VyCFC8k8reFtD36HBem5-zdvM1es30EwU-08U_07jcQU6eW3YVlKfmpCKC5SwlMGYdhyr1gvRTqE4ezyOMflXaH-v4EESZJ2egMfgV5UL0NqR7VzCb_h1oiJxEOD-sz7641q8BKnOwA7zIRNB7wnD0HOwbh3x54yRd_fUQVgll0PACJ9CkPau8SkCqdPlpobWNtG0fORLiEoTibG0XwF3tvB8ppMcupKKz6s84NcF1Nhwahp1IU6_0znJ4JmLO8lUWICemPyljYFnz5HU6WGhP5-Z5UOO5-uW0Sp5_Zl7mgWbtUZuu4QIVX3dZd87Ey3nyfR2WxbImgqrYYiHIph0qTeMFQAZA","displayName":"Alger Ivan","photoURL":"https://lh3.googleusercontent.com/a-/AOh14GgE5rB5uKhHf-9lO9XdsVkoo-AwdRFiBCXb17lL9A=s96-c"}';
  cy.visit('http://localhost:3000/dashboard');
  window.localStorage.setItem('user', userInfo);
  cy.wait(1000);
  cy.get('div.sticky > input').should('have.length', 1);
});
