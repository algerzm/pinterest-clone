/* eslint-disable no-undef */
it('Show error color on email input with not valid name', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="email"]').type('cha');
  cy.get('input[name="email"]').should('have.class', 'outline-red-600');
});

it('Show error color on password input with not valid name', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="password"]').type('cha');
  cy.get('input[name="password"]').should('have.class', 'outline-red-600');
});

it('Not to show error color if email is valid', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="email"]').type('algerivan@gmail.com');
  cy.get('input[name="email"]').not('.outline-red-600');
});

it('Not to show error color if password is valid', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="password"]').type('abc12345A');
  cy.get('input[name="password"]').not('.outline-red-600');
});

it('Should enable sign up button when form is valid', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="email"]').type('algerivan@gmail.com');
  cy.get('input[name="password"]').type('abc12345A');
  cy.get('#signin-button').should('not.be.disabled');
});

it('Should show spinner when signup', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="email"]').type('elchepo21_@hotmail.com');
  cy.get('input[name="password"]').type('abc12345A');
  cy.get('#signin-button').should('not.be.disabled');
  cy.get('#signin-button').click();
  cy.get('svg').should('have.class', 'animate-spin');
});
