/* eslint-disable no-undef */
it('Loads auth page succesfully', () => {
  cy.visit('http://localhost:3000/login');
});

it('Redirects from / to /login', () => {
  cy.visit('http://localhost:3000/');
  cy.url().should('include', '/login');
});

it('Shows Signup form first', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('h3').should('contain.text', 'Create Account');
  cy.get('button').should('contain.text', 'Sign Up');
});

it('Signup form shows signup button disabled', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#signup-button').should('be.disabled');
});

it('Login form shows login button disabled', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();

  cy.get('#signin-button').should('be.disabled');
});

it('Sign In button toggle to login form', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('h3').should('contain.text', 'Login with');
});

it('Sign in button toggle to login form and viceversa', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('h3').should('contain.text', 'Login with');
  cy.get('#go-signup-button').click();
  cy.get('h3').should('contain.text', 'Create Account');
});

it('Should redirect to /dashboard when sign up', () => {
  const date = new Date();
  const dateTime = date.getTime();
  cy.visit('http://localhost:3000/login');
  cy.get('input[name="name"]').type('MrNavi');
  cy.get('input[name="email"]').type(`cytest${dateTime}@gmail.com`);
  cy.get('input[name="password"]').type('abc12345A');
  cy.get(':checkbox').check();
  cy.get('#signup-button').should('not.be.disabled');
  cy.get('#signup-button').click();
  cy.intercept('http://localhost:3000/dashboard').as('dashboard');
  cy.wait(5000);
  cy.url().should('include', '/dashboard');
});

it('Should redirect to /dashboard when login', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="email"]').type('elchepo21_@hotmail.com');
  cy.get('input[name="password"]').type('abc12345A');
  cy.get('#signin-button').should('not.be.disabled');
  cy.get('#signin-button').click();
  cy.intercept('http://localhost:3000/dashboard').as('dashboard');
  cy.wait(5000);
  cy.url().should('include', '/dashboard');
});

it('Should show successfull login alert when login', () => {
  cy.visit('http://localhost:3000/login');
  cy.get('#go-login-button').click();
  cy.get('input[name="email"]').type('elchepo21_@hotmail.com');
  cy.get('input[name="password"]').type('abc12345A');
  cy.get('#signin-button').should('not.be.disabled');
  cy.get('#signin-button').click();
  cy.intercept('http://localhost:3000/dashboard').as('dashboard');
  cy.get('#swal2-html-container').should(
    'contain',
    'You have been authenticated successfully!'
  );
});

it('Should show alert when signup', () => {
  const date = new Date();
  const dateTime = date.getTime();
  cy.visit('http://localhost:3000/login');
  cy.get('input[name="name"]').type('MrNavi');
  cy.get('input[name="email"]').type(`cytest${dateTime}@gmail.com`);
  cy.get('input[name="password"]').type('abc12345A');
  cy.get(':checkbox').check();
  cy.get('#signup-button').should('not.be.disabled');
  cy.get('#signup-button').click();
  cy.get('#swal2-html-container').should(
    'contain',
    'You have been authenticated successfully!'
  );
});
