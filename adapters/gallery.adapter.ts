import { ResponseImage } from '@/models';

function removeVideosFromImageArray(images: ResponseImage[]) {
  return [...images].filter((image) => {
    if (image) {
      if (image.type !== 'video/mp4') return true;
      return false;
    }
    return false;
  });
}

function flatImageArray(images: ResponseImage[]): ResponseImage[] {
  return [...images].flat(Infinity);
}

const extractGalleryImages = function extractGalleryImages(gallery: any) {
  let imagesToAdapt: ResponseImage[] = [];

  gallery.forEach((element: any) => {
    if (imagesToAdapt.length < gallery.length) {
      imagesToAdapt = [...imagesToAdapt, element.images];
    }
  });

  return removeVideosFromImageArray(flatImageArray(imagesToAdapt));
};

export default extractGalleryImages;
