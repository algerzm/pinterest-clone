import { AdaptedImage, ResponseImage } from '@/models';

const createAdaptedImages = (images: ResponseImage[]): AdaptedImage[] => {
  const adaptedImages = images.map((image) => {
    return {
      id: image.id,
      title: image.title,
      description: image.description,
      width: image.width,
      height: image.height,
      views: image.views,
      tags: image.tags,
      link: image.link,
      type: image.type,
    };
  });

  return adaptedImages;
};

export default createAdaptedImages;
