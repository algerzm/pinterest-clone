import { User as FirebaseUser } from '@firebase/auth';
import { User } from '@/models';

const createAdaptedUser = (user: FirebaseUser): User => {
  return {
    id: user.uid,
    name: user.displayName || '',
    email: user.email || '',
    refreshToken: user.refreshToken,
    displayName: user.displayName || '',
    photoURL: user.photoURL || '',
  };
};

export default createAdaptedUser;
