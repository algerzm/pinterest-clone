import { User } from './user.model';

export interface GlobalState {
  user: User;
  authForm: string;
  loading: boolean;
  authError: boolean;
  authErrorMessage: string;
}
