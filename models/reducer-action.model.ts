/* eslint-disable @typescript-eslint/no-explicit-any */
export interface ReducerAction {
  type: string;
  payload: any;
}
