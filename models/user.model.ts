export interface User {
  id: string;
  name?: string;
  email: string;
  refreshToken: string;
  displayName?: string;
  photoURL?: string;
}
