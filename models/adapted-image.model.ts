export interface AdaptedImage {
  id: string;
  title: string;
  description: string;
  width: number;
  height: number;
  views: number;
  tags: string[];
  link: string;
  type: string;
}
